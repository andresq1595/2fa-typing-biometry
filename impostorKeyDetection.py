import keyboard
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import statistics 
import math


        

##globals
keyStrokes = []
key_Up = []
key_Down = []
desynchronized = []
waiting = []



####FIRESTORE######
def credentialsExists(db,phoneNumber, password):
    #print(phoneNumber, password)
    docs = db.collection(u'users').where(u'phoneNumber', u'==' , phoneNumber).where(u'password',u'==', password)
    dataDocs = docs.stream()
    for doc in dataDocs:
        #print('aqui')
        #print(f'{doc.id} => {doc.to_dict()}')
        return doc, True
    #print('sali')
    return None ,False

def connectFirestore():
    cred = credentials.Certificate('./key.json')
    firebase_admin.initialize_app(cred)
    db = firestore.client()
    return db

def updateSequence(db,id):
    doc = db.collection(u'users').document(id)
    sequence = formatSequence(passSequence)
    doc.set({u'sequence': sequence}, merge = True)


def formatSequence(sequence):
    sequenceHelper = []
    for i in sequence:
        if i.name != "enter" or i.name == "backspace":
            data = {"name": i.name, "event_type": i.event_type}
            #print (data)
            sequenceHelper.append(data)
    return sequenceHelper
    
 
    

#################################

def updateEmptyRythm(db,id,key_Down):
    doc = db.collection(u'users').document(id)
    rythm = []
    print('LEEEN')
    print(len(key_Down))
    for i in range(len(key_Down)):
        if(i != len(key_Down)-1):
            print(i)
            gapValue = key_Down[i+1]['time'] - key_Down[i]['time']
            doc.set({str(i)+u'gapRythm': [gapValue]}, merge = True)

def updateRythm(db, originalUserDocId, impostorDocId, currRythm):
    print('entre a update')
    impostorDoc = db.collection(u'users').document(impostorDocId)
    originalUserDoc = db.collection(u'users').document(originalUserDocId)

    existingRelation, relationId = existingImpostorRelation(db,impostorDoc, originalUserDoc)
    print(existingRelation)
  
    if (not existingRelation):
        data = {"impostorDoc": impostorDoc, "originalUserdoc": originalUserDoc }
        db.collection(u'impostorRelation').document().set(data, merge = True)
        updateRythm(db, originalUserDocId, impostorDocId, currRythm)

    relationDoc = db.collection(u'impostorRelation').document(str(relationId))                                                                           
    for i in range(len(currRythm)):
        if(i != len(key_Down)-1):
            newData = []
            try:
                currData = relationDoc.get().to_dict()[str(i)+'gapRythm']
                print('CURR')
                print(currData)
                print(currRythm[i]['time'])
                newData = currData + [currRythm[i+1]['time'] - currRythm[i]['time']]
            except Exception:
                newData = [currRythm[i+1]['time'] - currRythm[i]['time']]
                pass

           
            relationDoc.set({str(i)+u'gapRythm': newData}, merge = True)


def existingImpostorRelation(db,impostorDoc, originalUserDoc):
    impostorRelation = db.collection(u'impostorRelation')
    query = impostorRelation.where(u'impostorDoc',u'==', impostorDoc).where(u'originalUserdoc',u'==',  originalUserDoc)
    docs = query.stream()
    for doc in docs:
        print('entre a for')
        print(f'{doc.id} => {doc.to_dict()}')
        print(doc.id)
        return True, doc.id
    return False, -1



def compareRythm(db, originalUserDocId, impostorDocId, currRythm):
    largestCoeficient = 0
   
    originalUserDoc = db.collection(u'users').document(originalUserDocId)
    for i in range(len(currRythm)):
        if(i != len(currRythm)-1):
            currData = originalUserDoc.get().to_dict()[str(i)+'gapRythm']
            print(currData)
            print(i)
            print(currRythm[i]['time'])
            newData = currData + [currRythm[i+1]['time'] - currRythm[i]['time']]
            if len(currData) < 2:
                originalUserDoc.set({str(i)+u'gapRythm': newData}, merge = True)
                print('Need more rythm data for ' ,i , 'gap')
            else:
                stdvCurrData = statistics.stdev(currData)
                stdvNewData = statistics.stdev(newData)
                meanCurrData = statistics.mean(currData)
                meanNewData = statistics.mean(newData)
                varianceCoeficientCurrData = stdvCurrData / meanCurrData
                varianceCoeficientNewData = stdvNewData / meanNewData
                largestCoeficient = largestCoeficient + 1 if varianceCoeficientCurrData > varianceCoeficientNewData else largestCoeficient
    similarityPercentage = (largestCoeficient / (len(currRythm)-1) ) * 100
    print(similarityPercentage)
    updateRythm(db, originalUserDocId, impostorDocId, currRythm)
    if(similarityPercentage < 60):
        return False

    return True

def updateKeyStrokeData(db, originalUserDocId, impostorDocId, strokes):

    impostorDoc = db.collection(u'users').document(impostorDocId)
    originalUserDoc = db.collection(u'users').document(originalUserDocId)

    existingRelation, relationId = existingImpostorRelation(db,impostorDoc, originalUserDoc)
    print(existingRelation)
    print(relationId)

    if (not existingRelation):
        data = {"impostorDoc": impostorDoc, "originalUserdoc": originalUserDoc }
        db.collection(u'impostorRelation').document().set(data, merge = True)
        updateKeyStrokeData(db, originalUserDocId, impostorDocId, strokes)
    relationDoc = db.collection(u'impostorRelation').document(relationId)
    print(relationDoc)
    for i in range (len(strokes)):
        newData = []
        try:
            currData = relationDoc.get().to_dict()[strokes[i]['name']]
            print('CURR')
            print(currData)
            print(strokes[i]['time'])
            newData = currData + [strokes[i]['time']]
        except Exception:
            newData = [strokes[i]['time']]
            pass

        print(newData)
        relationDoc.set({strokes[i]['name']: newData}, merge = True)
    
    
    
                

def compareStrokeData(db, originalUserDocId, impostorDocId, strokes):
    doc = db.collection(u'users').document(originalUserDocId)
    falacies = 0
    for i in strokes:
        currData = doc.get().to_dict()[i['name']]
        array = currData + [i['time']]
        stdvCurrData = statistics.stdev(currData)
        stdvArray = statistics.stdev(array)
        print({i['name']})
        print(stdvArray)
        allowedDifference = stdvCurrData * .35
        lowBoundary = stdvCurrData - allowedDifference
        topBoundary = stdvCurrData + allowedDifference
        print(lowBoundary)
        print(topBoundary)
        if (stdvArray < lowBoundary or stdvArray > topBoundary):
            falacies = falacies + 1
   
    updateKeyStrokeData(db, originalUserDocId, impostorDocId, strokes)
    print('Falacies', falacies)
    allowedFallacies = math.ceil(float(len(strokes)) * .40 )

    grade = ((100 * (len(strokes)-falacies))/len(strokes))
    print(grade)
    if (allowedFallacies > falacies):
        return True, grade
    
    return False, grade
    



def reverseSync():
    if desynchronized:
        for i in range(len(waiting)):
            for j in range(len(desynchronized)):
                #print(waiting[i]['name'])
                #print(desynchronized[j]['time'])
                if waiting[i]['name'] == desynchronized[j]['name']:
                    key_Up.append(desynchronized[j])
                    waiting.pop(i)
                    desynchronized.pop(j)
                    return reverseSync()
    elif desynchronized == []:
        #print(waiting)
        #print(desynchronized)
        return False
        
                

def processRecord(recorded):
        #Biometria del tecleo
        
        ###############process record############
        last = ''
        for event in recorded:
            eventObject = {"name":event.name, "time": event.time}
            if(event.name == 'enter' or event.name == 'backspace'):
                continue
            if event.event_type == 'down':
                #print("DOWN" + event.name)
                key_Down.append(eventObject)
                if(last != eventObject):
                    waiting.append(eventObject)
                last = eventObject

            else:
                #print("UP" + event.name)
                if waiting[0]['name'] != eventObject['name'] :
                    #print("Desyncrhonized", event.name)
                    desynchronized.append(eventObject)
                elif waiting[0]['name'] == eventObject['name'] :
                    #print("Syncrhonized", event.name)
                    key_Up.append(eventObject)
                    waiting.pop(0)
                    reverseSync()

        ############################################

        ################Validate #########
        print("KEY UP" + str(len(key_Up)))
        print("KEY_DOWN " + str(len(key_Down)))

        #####################################

        #####Calculate
        for i in range(len(key_Up)):
            #print("UP"+key_Up[i].name, key_Up[i].time)
            #print("DOWN"+key_Down[i].name, key_Down[i].time)
            eventObject = {"name": key_Up[i]['name'], "time": (key_Up[i]['time'] - key_Down[i]['time'])}
            keyStrokes.append(eventObject)
        
        return keyStrokes
        #####################


db = connectFirestore()
impostorDocId = input("Escribe tu id de documento de usuario Impostor, despues teclea 'Enter' \n")
phoneNumber=input("Escribe tu numero de telefono, despues teclea 'Enter' \n")
#validamos y obtenemos cuenta de ese numero y contrasena
print("Escribe tu contrasena, despues teclea 'Enter'\n")
passSequence = keyboard.record(until='enter')
#keyboard.play(passSequence)
passwordValue = input("")
passPress = processRecord(passSequence)
##Get user where phone=phoneNumber && password=passwordValue(firestore)
originalUserDoc, FFA = credentialsExists(db,phoneNumber, passwordValue)
if FFA == False:
    print('Las credenciales proporcionadas son incorrectas, intenta de nuevo.')
    exit()
else: 
    currentSequence = originalUserDoc.to_dict()['sequence']
    
    formatPassSequence = formatSequence(passSequence)
    sequenceMatches = currentSequence == formatPassSequence ##Refactor this to allow some changes
    if not sequenceMatches:
        print('\nSabemos que eres tu, ayudanos a verificarlo escribiendo con la misma secuencia que estableciste desde las primeras 2 entradas.')
        exit()


    print(key_Down)
    compareRythm(db,originalUserDoc.id, impostorDocId, key_Down)
    strokeAuthenticated, grade = compareStrokeData(db,originalUserDoc.id, impostorDocId, passPress)

    # if grade >= 80:
    #     print('You are in')
    # elif strokeAuthenticated and grade < 80:
    #     print
    #     ##CheckRythm with confidence
    # elif (not strokeAuthenticated):
    #     ##CheckRythm without confidence

    # print(temp)
    

