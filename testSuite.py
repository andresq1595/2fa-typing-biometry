import keyboard
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import statistics 
import math

statistics.math


def connectFirestore():
    cred = credentials.Certificate('./key.json')
    firebase_admin.initialize_app(cred)
    db = firestore.client()
    return db

def getRelationDoc(originalUserId, impostorUserId):
    user = db.collection(u'users')
    originalUserDoc = user.document(originalUserId)
    impostorUserDoc = user.document(impostorUserId)

    impostorRelation = db.collection(u'impostorRelation')
    query = impostorRelation.where(u'impostorDoc',u'==', impostorUserDoc).where(u'originalUserdoc',u'==', originalUserDoc)
    docs = query.stream()
    for doc in docs:
        return True, doc.id
    return False, -1



#### cool
def testCompareRythm(originalUserId, relationId):
    userDoc = db.collection(u'users').document(originalUserId)
    relationDoc = db.collection(u'impostorRelation').document(str(relationId))
    userPassword = userDoc.get().to_dict()['password']
    largestCoeficient = 0
    for i in range (len(userPassword)):
        if (i != len(userPassword) - 1):
            originalGap = userDoc.get().to_dict()[str(i) + 'gapRythm']
            impostorGap = relationDoc.get().to_dict()[str(i) + 'gapRythm']
            compoundGap = originalGap + impostorGap
            stdvnOriginalGap = statistics.stdev(originalGap)
            stdvnCompoundGap = statistics.stdev(compoundGap)
            meanOriginalGap = statistics.mean(originalGap)
            meanCompoundGap = statistics.mean(compoundGap)
            varianceCoeficientOriginalGap = stdvnOriginalGap/meanOriginalGap
            varianceCoeficientCompoundGap = stdvnCompoundGap /meanCompoundGap 
            largestCoeficient = largestCoeficient + 1 if  varianceCoeficientOriginalGap > varianceCoeficientCompoundGap else largestCoeficient

            print("Original")
            print(originalGap)
            print("Impostor")
            print(impostorGap)
            print("Compound")
            print(compoundGap)
    similarityPercentage = (largestCoeficient / (len(userPassword) - 1)) * 100
    print(len(userPassword))
    print(len(userPassword)-1)
    print(largestCoeficient)
    print(similarityPercentage)

####not cool
def testCompareStroke(originalUserId, relationId):
    userDoc = db.collection(u'users').document(originalUserId)
    relationDoc = db.collection(u'impostorRelation').document(str(relationId))
    userPassword = userDoc.get().to_dict()['password']
    largestCoeficient = 0
    for char in userPassword:
        originalStroke = userDoc.get().to_dict()[char]
        impostorStroke = relationDoc.get().to_dict()[char]
        compoundStroke = originalStroke + impostorStroke
        stdvOriginalStroke = statistics.stdev(originalStroke)
        stdvCompoundStroke = statistics.stdev(compoundStroke)
        meanOriginalStroke = statistics.mean(originalStroke)
        meanCompoundStroke = statistics.mean(compoundStroke)
        varianceCoeficientOriginalStroke = stdvOriginalStroke/meanOriginalStroke
        varianceCoeficientCompoundStroke = stdvCompoundStroke/meanCompoundStroke
        largestCoeficient = largestCoeficient + 1 if  varianceCoeficientOriginalStroke > varianceCoeficientCompoundStroke else largestCoeficient

        print(char)
        print("Original")
        print(originalStroke)
        print("Impostor")
        print(impostorStroke)
        print(stdvOriginalStroke)
    similarityPercentage = (largestCoeficient / len(userPassword) ) * 100
    print(similarityPercentage)

    if(similarityPercentage < 60):
        return False

    
    return True


#Mejor que la del paper (solo en stroke)
def testCompareStrokeMyWay(originalUserId, relationId):
    userDoc = db.collection(u'users').document(originalUserId)
    relationDoc = db.collection(u'impostorRelation').document(str(relationId))
    userPassword = userDoc.get().to_dict()['password']
    falacies = 0
    for char in userPassword:
        originalStroke = userDoc.get().to_dict()[char]
        impostorStroke = relationDoc.get().to_dict()[char]
        compoundStroke = originalStroke + impostorStroke
        stdvOriginalStroke = statistics.stdev(originalStroke)
        stdvCompoundStroke = statistics.stdev(compoundStroke)
        allowedDifference = stdvOriginalStroke *.40
        lowBoundary = stdvOriginalStroke - allowedDifference
        topBoundary = stdvOriginalStroke + allowedDifference
        if (stdvCompoundStroke < lowBoundary or stdvCompoundStroke > topBoundary):
            falacies = falacies + 1
    print('Falacies', falacies)
    allowedFallacies = math.ceil(float(len(userPassword)) * .40)
    print(allowedFallacies)
            
    grade = ((100 * (len(userPassword)-falacies))/len(userPassword))
    if(allowedFallacies >= falacies):
        print(True)
        return True, grade
    else:
        print(False)
        return False, grade





#########################MAIN#########################################
db = connectFirestore()
originalUserId = input ("Dame el id documento del usuario original")
impostorUserId = input("Dame el id del usuario impostor")
validated,relationId = getRelationDoc(originalUserId, impostorUserId)
print(validated)
if (not validated):
    "No tengo datos para realizar ese test"
testType = input("Presiona 1 para incluir ritmo, Presiona 2 para incluir la presion del tecleo, Presiona 3 para combinar ambos algoritmos")
weight = {"stroke": 0, "rythm":0}
print(testType)
if (testType == '1'):
    print('ebtre')
    testCompareStroke(originalUserId, relationId)
elif (testType == '3'):
    rythmWeight = input("Dame el peso del ritmo")
    strokeWeight = input("Dame el peso de la presion del tecleo")
    weight = {"stroke": rythmWeight, "rythm":strokeWeight}


